package com.basics.week2.day5;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CustomExceptionImplementation {

	public static void main(String[] args) throws Exception{
		// TODO Auto-generated method stub
			List<String> list=new ArrayList<String>();
			list.add("JackFruit");
			list.add("Orange");
			list.add("Apple");
			list.add("Grapes");
			System.out.println("Enter the String :");
			Scanner sc=new Scanner(System.in);
			String a=sc.next();
			FruitChecker cf=new FruitChecker();
			cf.checkFruit(a, list);
		
	}
}
