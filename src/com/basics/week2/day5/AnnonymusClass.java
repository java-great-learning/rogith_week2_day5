package com.basics.week2.day5;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class AnnonymusClass extends Employee {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Employee> employees = new ArrayList<Employee>();
		employees.add(new Employee(1, "Shalini", "Mumbai"));
		employees.add(new Employee(5, "Alok", "Agra"));
		employees.add(new Employee(3, "Ambati", "Delhi"));
		employees.add(new Employee(2, "Deepak", "Pune"));
		employees.add(new Employee(4, "Nisha", "Chennai"));

		Collections.sort(employees);
		for(Employee employee : employees)
			System.out.println(employee);
		
		Collections.sort(employees, new Comparator<Employee>(){

			@Override
			public int compare(Employee o1, Employee o2) {
				// TODO Auto-generated method stub
				return o1.getCity().compareTo(o2.getCity());
			}
		});
		System.out.println();
		for(Employee employee : employees)
			System.out.println(employee);
		
	}

}
