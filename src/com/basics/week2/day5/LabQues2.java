package com.basics.week2.day5;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class LabQues2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a,in,bns;
		String b;
		String ans;
		List <String>lists=new ArrayList();
		Scanner sc=new Scanner(System.in);
		System.out.println("enter size :");
		a=sc.nextInt();
		sc.nextLine();
		System.out.println("enter elements :");
		while(a!=0)
		{
			b=sc.nextLine();
			lists.add(b);
			a--;
		}
		
		System.out.println("arraylist:");
		for(String no:lists)
        {
        	System.out.println(no);
        }
		
		Map<String,Integer>map=new HashMap<String,Integer>();
		System.out.println("enter size for map:");
		in=sc.nextInt();
		System.out.println("enter map elements:");
		for (int i = 0; i < in; i++) {
			 System.out.println("enter string:");
			 ans = sc.nextLine();
			 
			 System.out.println("enter int:");
	         bns = sc.nextInt();
	         sc.nextLine();
	         map.put(ans, bns);		
		}
		System.out.println("map:");
		for(String key:map.keySet())
		{
			System.out.println(key+" "+map.get(key));
		}
		Set<String> set=new HashSet<String>(lists);
		Iterator<String> i=set.iterator();  
		System.out.print("set:");
       /* while(i.hasNext())  
        {  
        System.out.println(i.next());  
        }  */
        for(String s:lists){  
            System.out.println(s);  
          }  

	}

}